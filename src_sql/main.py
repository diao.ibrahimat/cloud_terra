
import sqlalchemy
import os


def hello_sql(event, context):
   
    pool = sqlalchemy.create_engine(

        sqlalchemy.engine.url.URL.create(
            drivername="mysql+pymysql",
            username=os.environ["DB_USER"],
            password=os.environ["DB_PASSWORD"] ,
            database=os.environ["DB_NAME"],
            query={"unix_socket": os.environ["UNIX_SOCKET"]},
        ),
    )
   