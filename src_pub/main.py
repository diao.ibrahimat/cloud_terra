def hello_pubsub(event, context):

    import base64

    if 'data' in event:
        name = base64.b64decode(event['data']).decode('utf-8')
    else:
        name = 'Hello World'
    
    print(name)