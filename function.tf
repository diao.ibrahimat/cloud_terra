# Generates an archive of the source code compressed as a .zip file.

data "archive_file" "source_pubsub" {
  type        = "zip"
  source_dir = "${path.module}/src_pub"
  output_path = "${path.module}/function.zip"
  depends_on = [
    random_string.r
  ]
}

resource "random_string" "r" {
  length  = 16
  special = false
}
#archive gcs
data "archive_file" "source_gcs" {
  type        = "zip"
  source_dir = "${path.module}/src_gcs"
  output_path = "${path.module}/function_gcs.zip"
  depends_on = [
    random_string.c
  ]
}

resource "random_string" "c" {
  length  = 16
  special = false
}

#archive cloud sql
data "archive_file" "source_sql" {
  type        = "zip"
  source_dir = "${path.module}/src_sql"
  output_path = "${path.module}/function_sql.zip"
  depends_on = [
    random_string.m
  ]
}

resource "random_string" "m" {
  length  = 16
  special = false
}




# Add source code zip to the Cloud Function's bucket for pubsub
resource "google_storage_bucket_object" "zip" {
    source       = data.archive_file.source_pubsub.output_path
    content_type = "application/zip"

    # Append to the MD5 checksum of the files's content
    # to force the zip to be updated as soon as a change occurs
    name         = "src-${data.archive_file.source_pubsub.output_md5}.zip"
    bucket       = google_storage_bucket.function_bucket.name

    # Dependencies are automatically inferred so these lines can be deleted
    depends_on   = [
        google_storage_bucket.function_bucket,  # declared in `storage.tf`
        data.archive_file.source_pubsub
    ]
}

# Add source code zip to the Cloud Function's bucket
resource "google_storage_bucket_object" "zip_gcs" {
    source       = data.archive_file.source_gcs.output_path
    content_type = "application/zip"

    # Append to the MD5 checksum of the files's content
    # to force the zip to be updated as soon as a change occurs
    name         = "src-${data.archive_file.source_gcs.output_md5}.zip"
    bucket       = google_storage_bucket.function_bucket.name

    # Dependencies are automatically inferred so these lines can be deleted
    depends_on   = [
        google_storage_bucket.function_bucket,  # declared in `storage.tf`
        data.archive_file.source_pubsub
    ]
}

#add source 

resource "google_storage_bucket_object" "zip_sql" {
    source       = data.archive_file.source_sql.output_path
    content_type = "application/zip"

    # Append to the MD5 checksum of the files's content
    # to force the zip to be updated as soon as a change occurs
    name         = "src-${data.archive_file.source_sql.output_md5}.zip"
    bucket       = google_storage_bucket.function_bucket.name

    # Dependencies are automatically inferred so these lines can be deleted
    depends_on   = [
        google_storage_bucket.function_bucket,  # declared in `storage.tf`
        data.archive_file.source_sql
    ]
}

# Create the Cloud function triggered by a `Finalize` event on the bucket
resource "google_cloudfunctions_function" "function" {
    name                  = "function-pubsub"
    runtime               = "python37"  # of course changeable

    # Get the source code of the cloud function as a Zip compression
    source_archive_bucket = google_storage_bucket.function_bucket.name
    source_archive_object = google_storage_bucket_object.zip.name

    # Must match the function name in the cloud function `main.py` source code
    entry_point           = "hello_pubsub"
    
    # 
    event_trigger {
        event_type = "google.pubsub.topic.publish"
        resource   = "${var.project_id}-topic"
    }

    # Dependencies are automatically inferred so these lines can be deleted
    depends_on            = [
        google_storage_bucket.function_bucket,  # declared in `storage.tf`
        google_storage_bucket_object.zip
    ]
}

# Create the Cloud function triggered by a `Finalize` event on the bucket
resource "google_cloudfunctions_function" "function_gcs" {
    name                  = "function-trigger-on-gcs"
    runtime               = "python37"  # of course changeable

    # Get the source code of the cloud function as a Zip compression
    source_archive_bucket = google_storage_bucket.function_bucket.name
    source_archive_object = google_storage_bucket_object.zip_gcs.name

    # Must match the function name in the cloud function `main.py` source code
    entry_point           = "hello_gcs"
    
    # 
    event_trigger {
        event_type = "google.storage.object.finalize"
        resource   = "${var.project_id}-input"
    }

    # Dependencies are automatically inferred so these lines can be deleted
    depends_on            = [
        google_storage_bucket.function_bucket,  # declared in `storage.tf`
        google_storage_bucket_object.zip_gcs
    ]
}

#Create cloud function cloud sql 

resource "google_cloudfunctions_function" "function_sql" {
    name                  = "function-sql"
    runtime               = "python37"  # of course changeable

    # Get the source code of the cloud function as a Zip compression
    source_archive_bucket = google_storage_bucket.function_bucket.name
    source_archive_object = google_storage_bucket_object.zip_sql.name

    # Must match the function name in the cloud function `main.py` source code
    entry_point           = "hello_sql"
    
    # Add environment variables for Cloud SQL credentials
      event_trigger {
        event_type = "google.storage.object.finalize"
        resource   = "${var.project_id}-sql"
    }

    environment_variables = {
  
      DB_USER     = var.username
      DB_PASSWORD = var.passeword
      DB_NAME     = var.database
      UNIX_SOCKET = var.unisocket

    }

 
}
