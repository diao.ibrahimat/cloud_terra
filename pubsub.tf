
resource "google_pubsub_topic" "topic"  {
   name= "${var.project_id}-topic"
}

resource "google_cloud_scheduler_job" "job"  {
   
   name="${var.project_id}-job"
   description="test job"
   schedule="*/5 * * * *"
   
   pubsub_target {

    topic_name= "${google_pubsub_topic.topic.id}"
    data    = base64encode("hello-from-terraform")

    }

    }
